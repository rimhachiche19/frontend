export const DASHBOARD_SET_USERNAME = 'DASHBOARD.SET_USERNAME';
export const DASHBOARD_SET_ACTIVE_USERS = 'DASHBOARD.SET_ACTIVE_USERS';
export const DASHBOARD_SET_GROUP_CALL_ROOMS = 'DASHBOARD.SET_GROUP_CALL_ROOMS';
export const DASHBOARD_SET_EMAIL = 'DASHBOARD.SET_EMAIL';
export const DASHBOARD_SET_PASSWORD= 'DASHBOARD.SET_PASSWORD';
export const setUsername = (username) => {
  return {
    type: DASHBOARD_SET_USERNAME,
    username,
  };
}
;

export const setEmail = (email) => {
  return {
    type: DASHBOARD_SET_EMAIL,
    email,
  };
}
;
export const setPassword = (password) => {
  return {
    type: DASHBOARD_SET_PASSWORD,
    password,
  };
}
;

export const setActiveUsers = (activeUsers) => {
  return {
    type: DASHBOARD_SET_ACTIVE_USERS,
    activeUsers
  };
}
;

export const setGroupCalls = (groupCallRooms) => {
  return {
    type: DASHBOARD_SET_GROUP_CALL_ROOMS,
    groupCallRooms
  };
}
;
