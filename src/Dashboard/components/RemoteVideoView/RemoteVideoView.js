import React, { useRef, useEffect } from "react";

import { Avatar } from "antd";

const LocalVideoView = (props) => {
  const { remoteStream, callerUsername } = props;
  const remoteVideoRef = useRef();

  useEffect(() => {
    if (remoteStream) {
      const remoteVideo = remoteVideoRef.current;
      remoteVideo.srcObject = remoteStream;

      remoteVideo.onloadedmetadata = () => {
        remoteVideo.play();
      };
    }
  }, [remoteStream]);

  return (
    <>
      <video
        ref={remoteVideoRef}
        autoPlay
        muted
        className="card2"
        playsInline
        onClick={fullScreen}
      />

      <Avatar
        style={{
          /* backgroundColor: "#116", */
          width: "100%",
          position: "absolute",
          textAlign: "center",
          top: "91%",
          left: " 42%",
          transform: "translate(-50%, -50%)",
          wordWrap: "break-word",
        }}
        size={20}
        icon={!{ callerUsername }}
      >
        {callerUsername}
      </Avatar>
    </>
  );
};
const fullScreen = (e) => {
  const elem = e.target;

  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) {
    /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) {
    /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) {
    /* IE/Edge */
    elem.msRequestFullscreen();
  }
};

export default LocalVideoView;
