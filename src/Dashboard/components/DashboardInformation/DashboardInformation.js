import React from "react";

import "./DashboardInformation.css";
import MovingComponent from 'react-moving-text';
const DashboardInformation = (props) => {
  const { username } = props;
  return (
    <>
    
    <div className="dashboard_info_text_container">
     
    {/*  <FloatingLettersTextBuilder
        floatingSpeed={1000}
        lettersAppearanceDelay={250}
      >   <span className="dashboard_info_text_title">
       welcome in Vizia. Live
      </span>
</FloatingLettersTextBuilder> */}

<MovingComponent
  type="fadeInFromLeft"
  duration="1200ms"
  delay="0s"
  direction="normal"
  timing="ease"
  iteration="1"
  fillMode="none">
 <span className="dashboard_info_text_title">
      Hello {username} 
      </span>
</MovingComponent>

<MovingComponent
  type="fadeInFromLeft"
  duration="1200ms"
  delay="0s"
  direction="normal"
  timing="ease"
  iteration="1"
  fillMode="none">
 <span className="dashboard_info_text_title">
     Welcome in Vizia. Live
      </span>
</MovingComponent>
      
      <span className="dashboard_info_text_description">
       It’s Fast, It’s Live. Connect with Vizia live.
      </span>
     
    </div>
    </>
  );
};

export default DashboardInformation;
