import React, { useRef, useEffect } from 'react';
import './Video.css';
import { connect } from 'react-redux';

import {  setCallRejected, setLocalCameraEnabled, setLocalMicrophoneEnabled, setMessage } from '../../../store/actions/callActions';
import ConversationButtons from '../ConversationButtons/ConversationButtons';
import {  Avatar } from "antd";
const styles = {

  videoElement: {
    width: '100%',
    height: '100%'
  }
};

const LocalVideoView = props => {

  
    const {
      localStream,
      username,
      localCameraEnabled,
  } = props;
  const localVideoRef = useRef();

  useEffect(() => {
    if (localStream) {
      const localVideo = localVideoRef.current;
      
      localVideo.srcObject = localStream;

      localVideo.onloadedmetadata = () => {
        localVideo.play();
      };
    }
  }, [localStream]);
  const fullScreen = (e) => {
    const elem = e.target;

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      /* Chrome, Safari & Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
      /* IE/Edge */
      elem.msRequestFullscreen();
    }
  };
  return (

    <div
    style={{ textAlign: "center" }}
   
   
  >

    <div  className="video-avatar-container">
    {!localCameraEnabled &&<Avatar
              style={{
                /* backgroundColor: "#116", */
                width:'100%',
                position:"absolute",
                textAlign:"center",
                top: '50%',
               left:' 50%',  transform: 'translate(-50%, -50%)',
               wordWrap: 'break-word',
    
              }}
              size={20}
              icon={!( {username} )}
            >
              {username}
            </Avatar>}
 
      <video
      style={styles.videoElement} 
      ref={localVideoRef} 
      autoPlay
       muted
       className="video-active"
        playsInline
      onClick={fullScreen}
      />
         
       <ConversationButtons {...props} /></div>
   
    

  
      </div>

 

  
  );
};
function mapStoreStateToProps ({ call }) {
  return {
    ...call
  };
}
function mapDispatchToProps (dispatch) {
  return {
    hideCallRejectedDialog: (callRejectedDetails) => dispatch(setCallRejected(callRejectedDetails)),
    setCameraEnabled: (enabled) => dispatch(setLocalCameraEnabled(enabled)),
    setMicrophoneEnabled: (enabled) => dispatch(setLocalMicrophoneEnabled(enabled)),
    setDirectCallMessage: (received, content) => dispatch(setMessage(received, content))
  };
}
export default  connect(mapStoreStateToProps, mapDispatchToProps)(LocalVideoView);
