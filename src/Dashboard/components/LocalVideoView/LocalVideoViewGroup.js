import React, { useRef, useEffect } from 'react';
import './Video.css';
import { connect } from 'react-redux';
import {  setCallRejected, setLocalCameraEnabled, setLocalMicrophoneEnabled, setMessage } from '../../../store/actions/callActions';
import ConversationButtons from'../../../Dashboard/components/ConversationButtons/ConversationButtonsGroup';
//import { Card, Modal, Button, Input, notification, Avatar } from "antd";
const styles = {

  videoElement: {
    width: '100%',
    height: '100%'
  }
};

const LocalVideoView = props => {

  
    const {
      localStream,
  } = props;
  const localVideoRef = useRef();

  useEffect(() => {
    if (localStream) {
      const localVideo = localVideoRef.current;
      
      localVideo.srcObject = localStream;

      localVideo.onloadedmetadata = () => {
        localVideo.play();
      };
    }
  }, [localStream]);
  const fullScreen = (e) => {
    const elem = e.target;

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      /* Chrome, Safari & Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
      /* IE/Edge */
      elem.msRequestFullscreen();
    }
  };
  return (

    <div
    style={{ textAlign: "center" }}
   
   
  >

    <div  className="video-avatar-container">
      <video
      style={styles.videoElement} 
      ref={localVideoRef} 
      autoPlay
       muted
       className="video-active"
        playsInline
      onClick={fullScreen}

      />

 
<ConversationButtons {...props} groupCall /> </div>
   
    

  
      </div>

 

  
  );
};
function mapStoreStateToProps ({ call }) {
  return {
    ...call
  };
}
function mapDispatchToProps (dispatch) {
  return {
    hideCallRejectedDialog: (callRejectedDetails) => dispatch(setCallRejected(callRejectedDetails)),
    setCameraEnabled: (enabled) => dispatch(setLocalCameraEnabled(enabled)),
    setMicrophoneEnabled: (enabled) => dispatch(setLocalMicrophoneEnabled(enabled)),
    setDirectCallMessage: (received, content) => dispatch(setMessage(received, content))
  };
}
export default  connect(mapStoreStateToProps, mapDispatchToProps)(LocalVideoView);
