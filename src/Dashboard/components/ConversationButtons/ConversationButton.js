import React from 'react';

const styles = {
  button: {
    width: '45px',
    height: '35px',
    borderRadius: '40px',
    border: '2px solid #e6e5e8',
    textDecoration: 'none',
    backgroundColor: '#fefefe',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '4%',
    marginRight: '4%',
    boxShadow: 'none',
    borderImage: 'none',
    borderStyle: 'none',
    borderWidth: '0px',
    outline: 'none',
    marginTop:'2%',
    marginBottom:'2%',

  }
};

const ConversationButton = (props) => {
  const { onClickHandler } = props;
  return (
    <button style={styles.button} onClick={onClickHandler}>
      {props.children}
    </button>
  );
};

export default ConversationButton;
