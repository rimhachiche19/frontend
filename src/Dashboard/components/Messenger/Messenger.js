import React, { useState, useEffect } from 'react';
import { sendMessageUsingDataChannel } from '../../../utils/webRTC/webRTCHandler';
import MessageDisplayer from './MessageDisplayer';
import { Card, Modal, Button, Input, notification, Avatar } from "antd";
import { UserOutlined, MessageOutlined } from "@ant-design/icons";
import './Messenger.css';


const Messenger = ({ message, setDirectCallMessage}) => {
  const [inputValue, setInputValue] = useState('');
let msg=[];


  const handleOnKeyDownEvent = (e) => {
    if (e.keyCode === 13) {
      sendMessageUsingDataChannel(inputValue);
      setInputValue('');
   //   Notifier();
    }
  };
  const Notifier = () => {     notification.open({
    placement:'topLeft',
    duration: 4.5,
    message: "",
    description: `New Msg: ${message.content}`,
    icon: <MessageOutlined style={{ color: "black" }} />,
    className: 'custom-class',
    style: {
     color:'black',
     
    },


  });}
 /*  useEffect(() => {
    if (message.received) {
     Notifier();
      setTimeout(() => {
       
        setDirectCallMessage(false, '');
        console.log('m2',message)
      }, [3000]);
    }
  }, [message.received]); */

  return (
    <>
      <input
        className='messages_input'
        type='text'
        value={inputValue}
        onChange={(e) => { setInputValue(e.target.value); 
        msg.push(inputValue);
      }}
        onKeyDown={handleOnKeyDownEvent}
        placeholder='Type your message'
      />
      {message.received && <MessageDisplayer message={message.content} />}
    </>
  );
};

export default Messenger;
