import React, { useEffect } from "react";
import logo from "../resources/logo.png";
import ActiveUsersList from "./components/ActiveUsersList/ActiveUsersList";
import * as webRTCHandler from "../utils/webRTC/webRTCHandler";
import * as webRTCGroupHandler from "../utils/webRTC/webRTCGroupCallHandler";
import DirectCall from "./components/DirectCall/DirectCall";
import { connect } from "react-redux";
import DashboardInformation from "./components/DashboardInformation/DashboardInformation";
import { callStates } from "../store/actions/callActions";
import GroupCallRoomsList from "./components/GroupCallRoomsList/GroupCallRoomsList";
import GroupCall from "./components/GroupCall/GroupCall";
import imagee from "../resources/imagee.png";
import "./Dashboard.css";
import { useHistory } from "react-router-dom";

const Dashboard = (props) => {
  const history = useHistory();

  useEffect(() => {
    webRTCHandler.getLocalStream();
    webRTCGroupHandler.connectWithMyPeer();
  }, []);
  const { username, callState } = props;
  return (
    <div className="dashboard_container background_main_color">
      <div className="dashboard_left_section">
        <span
          style={{
            paddingTop: "1%",
            paddingLeft: "4%",
            fontSize: "25px",
            display: "flex",
            lineheight: "1",
            fontFamily: "Rubik , sans-serif",
            position: "absolute",
            fontWeight: "bold",
          }}
        >
          Vizia.
        </span>

        {callState !== callStates.CALL_IN_PROGRESS && (<span
          style={{
            paddingTop: "1%",
            marginLeft: "107%",
            paddingLeft: "50px",
            fontSize: "20px",
            fontWeight: "bolder",
            display: "block",
            lineheight: "1",
            fontFamily: "Rubik , sans-serif",
          }}
          onClick={() => {
            history.push("/signup");
          }}
        >
          Logout
        </span>)}

        <div className="dashboard_content_container ">
          <DirectCall username={username} />
          <GroupCall />
          {callState !== callStates.CALL_IN_PROGRESS && (
            <DashboardInformation username={username} />
          )}
          
        </div>

        {callState !== callStates.CALL_IN_PROGRESS && (
          <div className="dashboard_rooms_container background_secondary_color">
            <h1 style={{ marginLeft: "15px", marginRight: "15px" }}>
              Active Rooms
            </h1>
            <GroupCallRoomsList />
          </div>
        )}
        {callState !== callStates.CALL_IN_PROGRESS &&
          callState !== callStates.CALL_REQUESTED && (
           <div className="welcome-thumb ">
              <img src={imagee} alt="" />
            </div>
          )}
      </div>

      <div id="drawersContainer">
        <div id="rightDrawer" class="drawer">
          {callState !== callStates.CALL_IN_PROGRESS && (
            <input id="rightDrawerCheck" type="checkbox" />
          )}
          {callState !== callStates.CALL_IN_PROGRESS && (
            <div id="rightDrawerContents" class="drawerContents">
              <ActiveUsersList />
            </div>
          )}
          {callState !== callStates.CALL_IN_PROGRESS && (
            <img className="dashboard_logo_image" alt="img" src={logo} />
          )}
          {callState !== callStates.CALL_IN_PROGRESS && (
            <label
              id="rightDrawerPull"
              class="drawerPull"
              for="rightDrawerCheck"
            ></label>
          )}
        </div>
      </div>

      {/*   <div className='dashboard_right_section '>
        <div className='dashboard_active_users_list'>
          <ActiveUsersList />
        </div>
        <div className='dashboard_logo_container'>
          <img className='dashboard_logo_image' alt='img' src={logo} />
        </div>
      </div>  */}
    </div>
  );
};

const mapStateToProps = ({ call, dashboard }) => ({
  ...call,
  ...dashboard,
});

export default connect(mapStateToProps)(Dashboard);
