import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { useEffect } from 'react';
import { connectWithWebSocket } from './utils/wssConnection/wssConnection';
import Dashboard from './Dashboard/Dashboard';
import LoginPage from './LoginPage/LoginPage';
import SignInPage from './LoginPage/SignInPage';

function App () {
  useEffect(() => {
    connectWithWebSocket();
  }, []);

  return (
    <Router>
      <Switch>
        <Route path='/dashboard' component={Dashboard}/>
         
        <Route exact path='/signup' component={LoginPage}/>

        <Route exact path='/Signin' component={SignInPage}/>
        
      </Switch>
    </Router>
  );
}

export default App;
