import React, { useState } from "react";
import { connect } from "react-redux";
import Vizia from "../resources/Vizia.png";
import EmailInput from "./components/EmailInput";
import PasswordInput from "./components/PasswordInput";
import UsernameInput from "./components/UsernameInput";
import SubmitButton from "./components/SubmitButton";
import { useHistory } from "react-router-dom";
import {
  setUsername,
  setEmail,
  setPassword,
} from "../store/actions/dashboardActions";
import { registerNewUser } from "../utils/wssConnection/wssConnection";
import "./LoginPage.css";

const SignInPage = ({ saveUsername, saveEmail, savePassword }) => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const handleSubmitButtonPressed = () => {
    registerNewUser(username, email, password);
    saveUsername(username);
    saveEmail(email);
    savePassword(password);
    history.push("/dashboard");
  };

  return (
    <div className="login-page_container background_main_color">
      <div className="login-page_login_box background_secondary_color">
        <div className="login-page_logo_container">
          <img
            className="login-page_logo_image"
            src={Vizia}
            alt="VideoTalker"
          />
        </div>
        <div className="login-page_title_container">
          <h2>Please Sign In</h2>
          <p>
            If you haven't an account just <a href="/Signup">Sign Up</a>
          </p>
        </div>
        <UsernameInput username={username} setUsername={setUsername} />
        <PasswordInput password={password} setPassword={setPassword} />
        <SubmitButton handleSubmitButtonPressed={handleSubmitButtonPressed} />
      </div>
    </div>
  );
};

const mapActionsToProps = (dispatch) => {
  return {
    saveUsername: (username) => dispatch(setUsername(username)),
    saveEmail: (email) => dispatch(setEmail(email)),
    savePassword: (password) => dispatch(setPassword(password)),
  };
};

export default connect(null, mapActionsToProps)(SignInPage);
