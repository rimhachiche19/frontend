import React from 'react';

const EmailInput = (props) => {
  const { email, setEmail } = props;

  return (
    <div className='login-page_input_container'>
      <input
        placeholder='Enter your Email'
        type='text'
        value={email}
        onChange={(event) => { setEmail(event.target.value); }}
        className='login-page_input  text_main_color'
      />
    </div>
  );
};

export default EmailInput;
