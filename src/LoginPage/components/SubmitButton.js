import React from 'react';

const SubmitButton = ({ handleSubmitButtonPressed }) => {
  return (
    <div className='login-page_button_container'>
      <button
        className='login-page_button  text_main_color'
        onClick={handleSubmitButtonPressed}
      >
        Start using Vizia. Live
      </button>
    </div>

  );
};

export default SubmitButton;


