import React from 'react';

const PasswordInput = (props) => {
  const { password, setPassword } = props;

  return (
    <div className='login-page_input_container'>
      <input
        placeholder='Enter your password'
        type='password'
        value={password}
        onChange={(event) => { setPassword(event.target.value); }}
        className='login-page_input text_main_color'
      />
    </div>
  );
};

export default PasswordInput;
